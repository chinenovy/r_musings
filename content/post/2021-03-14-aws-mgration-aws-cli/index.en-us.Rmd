---
title: AWS mgration. AWS CLI
author: YC
date: '2021-03-14'
slug: []
categories:
  - AWS
tags: []
keywords:
  - tech
summary: "Notes on AWS migration from a total AWS beginner"
thumbnailImage: "./post/2021-03-14-aws-mgration-aws-cli/images/great_migration.png"
thumbnailImagePosition: left
#autoThumbnailImage: yes
output:
  blogdown::html_page    
---


```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```
At HSS we started to map out our eventual AWS migration. Everything related to AWS
in this blog has been inspired by this "great migration". Although I have been 
using AWS for a few year my experience has been limited to serving some static 
files to the UCSC genomics browser using AWS web console. This is hardly enough
for what we want to achieve here.

Right now we are planing something much more complex that involves moving 
our entire sample storage, integrating sample files metadata into a self-updating 
database, and eventually integrating the storage and database with our data 
processing workflows. HSS data science will be involved in this but they have no 
experience with genomics data and workflow, I have this experience but no experience
with AWS so, we are making it up as we go along.

To start it all we would have stop relying on the Web management console (as much)
and switch to a more programmatic access. This means that we either do it with
the AWS command line interface (CLI) or Python using boto3 (or both). 

## 1. Set up CLI at local linux workstation and HPC

Since I have an account with AWS I will be using this account for my trials and errors,
first thin first I need to set up AWS CLI version 2 on my local workstation and
at HPC

AWS documentation is [here]("https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html")

Download the latest version of CLI2

```{bash, eval = F}
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install
```

curently located at ~/aws,
folowing installation you should receive the following message 
`You can now run: /usr/local/bin/aws --version`

confirm installation

```{bash, eval = F}
$ aws --version
aws-cli/2.1.29 Python/3.8.8 Linux/5.8.0-44-generic exe/x86_64.ubuntu.20 prompt/off
```

Now, we need to configure AWS CLI to access your AWS account. The idea is the same
as with normal linux. When you create your first account with [**AWS Management Console**](https://aws.amazon.com/console/)
this account has root priviliges. It is a bad practice to use you root account
security credential for anything that is not absolutely essential. 
So, you first should create your **IAM admin user and group**. Oh yeah, AWS love 
acronyms, half of the time I forget the meaning 20 seconds after I learned it: IAM
stands for Identity and Access Managment - an AWS service responsible for users 
and resource access management and security

Since I am doing it on MY account I would like to create an administrative **group**.
However, in order to do this you first need your security credentials to configure
AWS CLI access. THis is where you would have to use AWS console, because this is
the only way to get security keys and secret for either root user (not recommended)
or a IAM user with administrative priviliges for the first time

Information about [credential for CLI is here](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-quickstart.html#cli-configure-quickstart-creds)

to configure AWS CLI you can use `aws configure` command. You will need the following
information to create a valid configuration:

  -Access key ID

  -Secret access key

  -AWS Region

  -Output format
  
### Access key ID
  If you need this for root account you can only get it from the web management console
  You have an Admin with root privileges (that would be me on this account) you
  will receive it from me

### Secret access key
  Same as above
  
### AWS region
  Your admin will tell you. On my account I use `us-east-1`.
  
### Output format
  Here are the options: **json, yaml, yaml-stream, text, table**. json is the
  default. I am sorrely temted to try either yaml or text. May be I will on other
  accounts

On my workstation I would like to have two profiles. The default is for my 
IAM user and an extra profile for root. Calling `aws configure` with no arguments
will create a default profile that is stored in `~/home/.aws` directory. 
The profiles are stored in two files `credentials` amd `config`. `credentials` contains  
Access key ID and Secret access key, config contains other information(region and 
output format). 

**credentials**
```{bash, eval = F}
[default]
aws_access_key_id = BLAHBLAHBLAHBLAH
aws_secret_access_key = moreCOM/plexBLAH?BLAH/blahblaHbLAHblahBlhahhhhaal
```

**config**
```{bash, eval = F}
[default]
region = us-east-1
output = json
```

If you want to add additional profiles use `aws configure --profile newProfileName`

Now, if everything worked correctly you can quickly test you setup by running 
some of the `aws iam` commands. This for example provides you a list of groups 
No this account number is not real. As you can see, it is json but you can change it

```{bash, eval = F}
$ aws iam list-groups
{
    "Groups": [
        {
            "Path": "/",
            "GroupName": "hssAdmin",
            "GroupId": "SOMELONGSTRING",
            "Arn": "arn:aws:iam::146428167169:group/hssAdmin",
            "CreateDate": "2021-03-08T17:34:11+00:00"
        }
    ]
}

```

Now, let create a new user account. You can either do it via management console 
or via CLI. So this is what I want to do

1) Create user `someuser` with tags. 
NOTE: Key and Value are separate by a comma, key-value pairs by a space
```{bash, eval = F}
aws iam create-user --user-name someuser --tags Key=name,Value=John_Doe Key=Email,Value=doej@hss.edu Key=secondaryEmail,Value=jdoe@gmail.com
```

2) Give the user access to the AWS Management Console (optional)
This requires a password, I also specified that the user is required to update 
password on first login. Password needs to conform AWS policies or you will receive an error
```{bash, eval = F}
aws iam create-login-profile --user-name someuser --password Hsomesmallleters! --password-reset-required
```

3) Give user a programmatic access
This requires Access key ID and a secret. 
```{bash, eval = F}
aws iam create-access-key --user-name someuser
```

4) Add user to an existing group (optional)
You might want to check what groups exist first
```{bash, eval = F}
aws iam list-groups
aws iam add-user-to-group --user-name someuser --group-name highAdmin
```

You can attach existing policies to an individual user but it is not recommended.
The best practice is to create policies for a group and attach user to a group
If your group does not have policies you can create them using using 
`aws iam create-policy`. This command takes a json document as an argument. 
You could find policy examples [here](https://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies_examples.html)
but it is easier to actually do it via a console and then save a json.  









